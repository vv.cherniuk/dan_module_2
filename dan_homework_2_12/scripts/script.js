const btn = Array.from(document.querySelectorAll(".btn"));

document.addEventListener("keydown", (event) => {
  btn.forEach((element) => {
    if (element.innerHTML.toUpperCase() == event.key.toUpperCase()) {
      element.classList.add("mark");
    } else {
      element.classList.remove("mark");
    }
  });
});
