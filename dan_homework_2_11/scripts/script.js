const input = document.querySelector("#input");
const inputConfirm = document.querySelector("#inputConfirm");
let mistakeMessage = document.querySelector(".mess");
let div = document.createElement("div");
let confirmDiv = document.createElement("div");

const picture = document.querySelector("#pic1");
picture.addEventListener("click", () => {
  picture.classList.toggle("fa-eye-slash");
  if (input.type === "password") {
    input.type = "text";
  } else {
    input.type = "password";
  }
});

const picture2 = document.querySelector("#pic2");
picture2.addEventListener("click", () => {
  picture2.classList.toggle("fa-eye-slash");
  if (inputConfirm.type === "password") {
    inputConfirm.type = "text";
  } else {
    inputConfirm.type = "password";
  }
});

inputConfirm.addEventListener("focus", () => {
  div.remove();
});

function deleteDiv() {
  confirmDiv.remove();
}

const button = document.querySelector(".btn");
button.addEventListener("click", (e) => {
  e.preventDefault();
  if (input.value === inputConfirm.value) {
    confirmDiv.setAttribute("class", "confirmDiv");
    confirmDiv.innerText = "Your are Welcome!";
    document.body.append(confirmDiv);
    setTimeout(deleteDiv, 2000);
  } else {
    div.style.color = "red";
    div.style.fontSize = "0.8rem";
    div.innerHTML = "Нужно ввести одинаковые значения";
    mistakeMessage.append(div);
  }
});
