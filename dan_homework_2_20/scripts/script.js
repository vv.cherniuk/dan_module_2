const vehicles = [
  {
    name: "Toyota",
    model: "Crown",
    year: 2019,
    color: "Red",
    contentType: { name: "en_US", type: "jstring" },
    locales: [
      { name: "en_US", description: "English" },
      { name: "fr_FR", description: "Francais" },
    ],
  },

  {
    name: "Kraisler",
    model: "Sportage",
    year: 2016,
    color: "Green",
    contentType: { name: "json", type: "jstring" },
    locales: [
      { name: "en_US", description: "English" },
      { name: "es_US", description: "Espanol" },
    ],
  },

  {
    name: "Lexus",
    model: "RX300",
    year: 2021,
    color: "Red",
    contentType: { name: "json", type: "jstring" },
    locales: [
      { name: "es_US", description: "Espanol" },
      { name: "fr_FR", description: "Francais" },
    ],
  },
];

function filterCollection(arr, keyWords, bool, ...rest) {
  let keyWordsArr = keyWords.split(" ");
  let pass = rest;

  const filterArr = arr.filter((element) => {
    
    let restKeyWords = [];

    function innerArr(data) {
      Object.values(data).forEach((el) => {
        if (Array.isArray(el)) {
          el.forEach((elem) => innerArr(elem));
        }
      });

      for (let val of pass) {
        if (Object.keys(data).includes(val)) {
          for (let keyWord of keyWordsArr) {
            if (Object.values(data).includes(keyWord)) {
              if (!restKeyWords.includes(keyWord)) {
                restKeyWords.push(keyWord);
              }
            }
          }
        }
      }
    }
    innerArr(element);
    return bool
      ? restKeyWords.length === keyWordsArr.length
      : restKeyWords.length > 0;
  });

  return filterArr;
}



console.log(
  filterCollection(
    vehicles,
    "Espanol",
    true,
    "name",
    "model",
    "color",
    "description",
    "contentType",
    "locales.description",
    "locales.name"
  )
);
