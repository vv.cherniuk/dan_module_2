function getListContent(arr, pos = document.body) {
  let ul = document.createElement("ul");

  newArr = arr.map((element) => {
    if (Array.isArray(element)) {
      return getListContent(element, ul);
    }

    let li = document.createElement("li");
    li.append(element);
    ul.append(li);
    return li;
  });
  pos.append(ul);
}

let pos = document.querySelector(".script-2");
const arr = ["Kiev", ["Brovary", "Irpin", "Borispol"], "Odessa", "Lviv"];

function timer(start, end) {
  getListContent(arr, pos);
  let current = start;
  let div = document.querySelector(".timer");
  div.style.display = "block";
  setTimeout(function go() {
    div.innerText = `00:0${current}`;
    if (current > end) {
      setTimeout(go, 1000);
    }
    if (current === end) {
      pos.remove();
    }
    current--;
  }, 1000);
}

timer(3, 0);
