let ol = document.createElement("ol");

function getListContent(arr, pos) {
  for (let key in arr) {
    if (Array.isArray(arr[key])) {
       getListContent(arr[key], pos);
    } else {
      let li = document.createElement("li");
      li.append(arr[key]);
      ol.append(li);
    }
  }
  let position = document.querySelector(pos);
  position.append(ol);
}


let pos = ".script-1";
const arr = ["Kiev", ["Borispol", "Brovary", "Irpin"], "Odessa", "Lviv"];

getListContent(arr, pos);
