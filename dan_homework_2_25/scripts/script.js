const display = document.querySelector(".display input");
const keys = document.querySelector(".keys");
const num = document.querySelector(".display span");

let a = "";
let b = "";
let oper = "";
let result = "";
let sumMemory = "0";

const operators = {
  "-": (a, b) => Number(a) - Number(b),
  "+": (a, b) => Number(a) + Number(b),
  "*": (a, b) => Number(a) * Number(b),
  "/": (a, b) => Number(a) / Number(b),
};

keys.addEventListener("click", numbersClick);
window.addEventListener("keydown", keyNumber);

function clear() {
  a = "";
  b = "";
  sumMemory = "";
  result = "";
  oper = "";
  num.style.display = "none";
  displayValue("0");
}

function displayValue(result) {
  display.value = Number(Number(result).toFixed(15));
}

function numbersClick(event) {
  let dataName = event.target.dataset.name;

  switch (dataName) {
    case "value":
      if (oper === "") {
        a += event.target.value;
        return (display.value = a);
      } else {
        b += event.target.value;
        return (display.value = b);
      }
    case "operand":
      return (oper = event.target.value);

    case "equals":
      result = operators[oper](a, b);
      a = result.toString();
      b = "";
      oper = "";
      return displayValue(result.toString());
    case "clear":
      return clear();
    case "operand-memory":
      return memory(event);
    default:
      return;
  }
}

function keyNumber(event) {
  let eventCode = event.code;
  let eventNumber = event.key;

  if (eventCode.includes("Digit") || eventCode.includes("Period")) {
    if (oper === "") {
      a += event.key;
      return (display.value = a);
    } else {
      b += event.key;
      return (display.value = b);
    }
  } else if (
    eventNumber.includes("*") ||
    eventNumber.includes("/") ||
    eventNumber.includes("-") ||
    eventNumber.includes("=")
  ) {
    if (eventNumber === "=") {
      return (oper = "+");
    }
    return (oper = event.key);
  } else if (eventNumber.includes("Enter")) {
    result = operators[oper](a, b);
    a = result.toString();
    b = "";
    oper = "";
    return displayValue(result.toString());
  } else if (eventCode.includes("KeyC")) {
    return clear();
  } else {
    return;
  }
}

function memory(event) {
  switch (event.target.value) {
    case "mrc":
      a = sumMemory;
      display.value = sumMemory;
      if (sumMemory === "0") {
        num.style.display = "none";
        clear();
      }
      sumMemory = "0";
      break;
    case "m-":
      num.style.display = "block";
      if (sumMemory === "0") {
        sumMemory = - Number(display.value);
      } else {
        sumMemory = Number(sumMemory) - Number(display.value);
      }
      displayValue(sumMemory);
      break;

    case "m+":
      num.style.display = "block";
      sumMemory = Number(sumMemory) + Number(display.value);
      displayValue(sumMemory);
      break;
  }
}
