/*1*/
let admin;
let name = 'Volodymyr';
admin = name;
alert(admin);

/*2*/
let days = 10;
const SECONDS_PER_DAY = 86400;
let seconds = days * SECONDS_PER_DAY;
alert(seconds);

/*3*/
let userAnswer = prompt('How old are you', '');
if (userAnswer !== null && userAnswer !=='' ) {
    alert(`Your are ${userAnswer} years old`);
}
else {
    alert("Your didn't enter your age!");
}