const button = document.querySelector("button");
const img = document.querySelector("figure img");

button.addEventListener("click", changeTheme);

if (sessionStorage.type === 'black') {
    document.body.classList.toggle("black-background");
    document.body.classList.toggle("black-color");
    img.classList.toggle("black-opacity");
    button.innerHTML = "White Theme";
    button.style.color = "black";
    button.style.backgroundColor = "white";
}

function changeTheme() {
  document.body.classList.toggle("black-background");
  document.body.classList.toggle("black-color");
  img.classList.toggle("black-opacity");
  if (button.innerHTML === "Black Theme") {
    button.innerHTML = "White Theme";
    button.style.color = "black";
    button.style.backgroundColor = "white";
    sessionStorage.setItem("type", "black");
  } else {
    button.innerHTML = "Black Theme";
    button.style.color = "white";
    button.style.backgroundColor = "black";
    sessionStorage.setItem("type", "white");
  }
}

console.log(sessionStorage.getItem("type"));
