const imagesToFade = Array.from(document.querySelectorAll("img"));
const buttonStop = document.querySelector(".stop");
const buttonStart = document.querySelector(".start");

let i = imagesToFade.length - 1;
let timerSpan = document.querySelector(".timer");
let duration = 3000; //устанавливает длительность переключения картинок

function carusel() {
  let startTime = Date.now();
  let timerID = setTimeout(function change() {
    let elapsTime = duration - Math.round(Date.now() - startTime);
    timerSpan.innerHTML = (elapsTime / 1000).toFixed(2);
    timerID = setTimeout(change, 100);
  }, 100);

  let timer = setTimeout(function changeImage() {
    imagesToFade[i].style.opacity = "0";
    i--;
    startTime = Date.now();
    if (i < 0) {
      imagesToFade.forEach((element) => {
        element.style.opacity = "1";
      });
      i = imagesToFade.length - 1;
    }
    timer = setTimeout(changeImage, duration);
  }, duration);

  buttonStop.addEventListener("click", () => {
    clearInterval(timer);
    clearInterval(timerID);
  });
}
const buttonWrapper = document.querySelector(".button-wrapper");

window.addEventListener("load", () => {
  setTimeout(() => {
    buttonWrapper.style.display = "flex";
  }, 3000);
});
buttonStart.addEventListener("click", carusel);

//Если необходимо начать смену картинок автоматически после загрузки страницы
//window.addEventListener('load', carusel);
