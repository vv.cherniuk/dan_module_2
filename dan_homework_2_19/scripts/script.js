function deadLineCount(speed, backLog, deadDate) {
  let fullSpeedPerDay = 0;
  for (let key in speed) {
    fullSpeedPerDay += speed[key];
  }
  let fullLog = 0;
  for (let key in backLog) {
    fullLog += backLog[key];
  }
  //вычисляем сколько целых рабочих смен по 8 часов необходимо и сколько дополнительно часов)
  let workTimeToEnd = fullLog / fullSpeedPerDay;
  let workDays = Math.trunc(workTimeToEnd); //целых смен
  let workHours = Math.floor((workTimeToEnd % 1) * 8); //часов

  //добавляем к текщей дате целые рабочие смены - не включая выходные 6/0)
  let endTime = new Date();
  for (let i = 1; i < workDays; ) {
    if (endTime.getDay() === 6 || endTime.getDay() === 0) {
      endTime.setDate(endTime.getDate() + 1);
    } else {
      endTime.setDate(endTime.getDate() + 1);
      i++;
    }
  }

  //добавляем к дате окончания оставшиеся часы
  endTime.setHours(endTime.getHours() + workHours);
  
  //подсчитываем хватит ли времени до дедлайна и выводим соответствующее сообщение
  let deadLineDate = new Date(deadDate);
  if (+endTime < +deadLineDate) {
    let daysToEnd = Math.floor(((+deadLineDate) - (+endTime)) / (1000 * 60 * 60 *24));
    alert(`Усі завдання будуть успішно виконані за ${daysToEnd} днів до настання дедлайну!`);
  } else {
    let addHours = Math.floor(((+endTime) - (+deadLineDate)) / (1000 * 60 * 60));
    alert(`Команді розробників доведеться витратити додатково ${addHours} годин після дедлайну, щоб виконати всі завдання в беклозі!`);
  }
}

const speed = [12, 8, 6, 3];
const backLog = [244, 149, 258, 56];
const deadLineDate = "2023-03-06T14:00:00.000";

deadLineCount(speed, backLog, deadLineDate);
