const button = document.querySelector("button");
const div = document.createElement("div");
const input = document.createElement("input");
const label = document.createElement("label");
const svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
const circle = document.createElementNS("http://www.w3.org/2000/svg", "circle");
const svgWrapper = document.createElement("div");

document.addEventListener("click", (event) => {
  if (event.target.innerHTML === "Намалювати коло") {
    addDiameter();
  } else if (event.target.innerHTML === "Намалювати!") {
    button.remove();
    div.remove();
    const widthDiv = document.createElement("div");
    widthDiv.append(...circleDraw());
    widthDiv.style.width = `${parseInt(input.value) * 10}px`;
    document.body.append(widthDiv);
    let circleColor = Array.from(document.querySelectorAll("circle"));

    circleColor.map((element) => {
      element.setAttribute("fill", randomColor());
    });
  } else if (event.target.hasAttribute("r")) {
    event.target.parentElement.parentElement.remove();
  }
});

function addDiameter() {
  label.innerHTML = "Введіть диаметер кола (px): ";
  button.innerHTML = "Намалювати!";
  label.append(input);
  div.append(label);
  button.after(div);
  input.after(button);
}

function circleDraw() {
  svg.setAttribute(
    "width",
    parseInt(input.value) / 2 + parseInt(input.value) / 2
  );
  svg.setAttribute(
    "height",
    parseInt(input.value) / 2 + parseInt(input.value) / 2
  );
  circle.setAttribute("r", parseInt(input.value) / 2);
  circle.setAttribute("cx", parseInt(input.value) / 2);
  circle.setAttribute("cy", parseInt(input.value) / 2);
  svg.append(circle);
  svgWrapper.append(svg);

  let arr = [];
  for (let i = 1; i <= 100; i++) {
    let divClone = svgWrapper.cloneNode(true);
    divClone.style.display = "inline-block";
    divClone.setAttribute("escape", "true");
    arr.push(divClone);
  }
  return arr;
}

function randomColor() {
  return `#${Math.floor(Math.random() * 1000000)}`;
}
