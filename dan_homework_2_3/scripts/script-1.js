/*
Завдання + проверка являетс ли введенное число целым

- Запрашивается число у пользователя и происходит проверка 
на целостность этого числа. Если число не целое - выводится 
сообщение о том, что число не целое и запрашивается число снова.
- После введения целого числа - находятся все числа в 
диапазоне от 0 до введенного пользователем числа, 
за исключением 0, так как любое число кратно 0.
- найденные кратные числа в диапазоне добавляются в массив, 
а затем выводятся на экран.
*/

let userNumber;
for (userNumber = prompt("Enter your Number", ""); userNumber % 1 !== 0; ) {
  alert("Your Number is not Integer");
  userNumber = prompt("Enter your Right Number", "");
}

let result = [];
for (let i = 0; i <= userNumber; i++) {
  let resultNumber = i / 5;
  if (Number.isInteger(resultNumber) && resultNumber !== 0) {
    result.push(i);
  }
}

if (result.length !== 0) {
  alert(`Числа, кратные 5: ${result}`);
} else {
  alert("Sorry, No Such Numbers");
}