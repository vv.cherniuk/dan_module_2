function fibo(f0, f1, n) {
  let a = f0;
  let b = f1;
  
  if (n < 0) {
    for (let i = 3; i >= n; i--) {
      let c = a + b;
      a = b;
      b = c;
    }
    return b;
  } else {
    for (let i = 3; i <= n; i++) {
      let c = a + b;
      a = b;
      b = c;
    }
    return b;
  }
}

console.log(fibo(1, 1, 10));
