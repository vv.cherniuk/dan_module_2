//1

const p = document.querySelectorAll('p');

function changeColor(element) {
    element.style.backgroundColor = '#ff0000';
}
p.forEach(changeColor);



//2

const optionsList = document.querySelector('#optionsList');
console.log(optionsList);


//3

const testParagraph = document.querySelectorAll('.testParagraph');

function changeContent(content) {
    content.innerText = 'This is a paragraph';
}

testParagraph.forEach(changeContent);


//4-5
const mainHeader = document.querySelector('.main-header');
const mainHeaderChildren = mainHeader.children;

for (let elem of mainHeaderChildren) {
    elem.classList.add('nav-item');
    console.log(elem);
}

//6
const sectionTitle = document.querySelectorAll('.section-title');

for (let elem of sectionTitle) {
    elem.classList.remove('section-title');
    console.log(elem);
}