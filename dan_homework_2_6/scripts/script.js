function createNewUser(firstName, lastName, userBirthday) {
  let newUser = {};
  newUser.getLogin = function () {
    let loginFirstName = this.firstName[0].toLowerCase();
    let loginLastName = this.lastName.toLowerCase();
    let login = loginFirstName + loginLastName;
    return login;
  };
  newUser.getAge = function () {
    let nowTime = new Date();
    let today = new Date(nowTime.getFullYear(), nowTime.getMonth(), nowTime.getDate());
    let birthdayArr = this.birthday.split('.');
    birthday = birthdayArr[2]+'-'+birthdayArr[1]+'-'+birthdayArr[0];
    let dob = new Date(birthday);
    let dobnow = new Date(today.getFullYear(), dob.getMonth(), dob.getDate());
    let userAge = today.getFullYear() - dob.getFullYear();
    if (today < dobnow) {
      userAge--;
    }
    return userAge;
  };
  newUser.getPassword = function () {
    let birthdayArr = this.birthday.split('.');
    let userPassword = this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + birthdayArr[2];
    return userPassword;
  }

  Object.defineProperty(newUser, "firstName", {
    get: function () {
      return firstName;
    },
    set: function (firstName) {
      firstName = firstName;
    },
  });
  Object.defineProperty(newUser, "lastName", {
    get: function () {
      return lastName;
    },
    set: function (lastName) {
      lastName = lastName;
    },
  });
  Object.defineProperty(newUser, "birthday", {
    get: function () {
      return userBirthday;
    },
    set: function () {
      birthday = userBirthday;
    },
  });

  return newUser;
}

function setFirstName() {
  let userNameFirst = prompt("Enter First Name", "");
  return userNameFirst;
}
function setLastName() {
  let userNameLast = prompt("Enter Last Name", "");
  return userNameLast;
}
function setUserBirthday() {
  let userBirthday = prompt("Enter your Birtthday", "dd.mm.yyyy");
  return userBirthday;
}


let newUser = createNewUser(setFirstName(), setLastName(), setUserBirthday());


console.log(newUser);
console.log(newUser.getAge());
console.log(newUser.getPassword());
