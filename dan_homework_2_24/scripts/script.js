const pageWrapper = document.querySelector(".page-wrapper");
const input = document.querySelector("input");
const label = document.querySelector("label");
const button = document.createElement("button");
const newGameButton = document.createElement("button");
const countButton = document.createElement("span");
const p = document.querySelector("p");

function addMineFields(fields) {
  const miner = document.createElement("div");
  const mineField = document.createElement("div");
  miner.classList.add("miner");
  pageWrapper.append(miner);
  mineField.classList.add("mine-field");
  let fieldsNumber = fields * fields;
  miner.style.width = `${fields * 20}px`;
  for (let i = 1; i <= fieldsNumber; i++) {
    const mineFieldClone = mineField.cloneNode(true);
    miner.append(mineFieldClone);
    label.remove();
  }
  mineGenerating();
}

function buttonStart() {
  button.setAttribute("type", "button");
  button.innerHTML = "START";
  label.append(button);
}

input.addEventListener("input", () => {
  if (document.querySelector("button") === null) {
    buttonStart();
  }
});

button.addEventListener("click", () => {
  if (input.value !== "" && input.value > 0) {
    addMineFields(input.value);
    newGameButton.innerHTML = "Start New Game";
    newGameButton.classList.add("button-start");
    p.append(newGameButton);
    newGameButton.append(countButton);
  }
});

newGameButton.addEventListener("click", () => {
  document.querySelector(".miner").remove();
  newGameButton.remove();
  input.value = "";
  pageWrapper.append(label);
});

function mineGenerating() {
  let mineGenerated = Array.from(document.querySelectorAll(".mine-field"));
  for (let i = 0; i <= Math.round((input.value * input.value) / 6); i++) {
    mineGenerated[
      Math.floor(Math.random() * (input.value * input.value))
    ].innerHTML = '<i class="fa-solid fa-star-of-life"></i>';
  }
  let x = 0;
  mineGenerated.forEach((element) => {
    if (element.innerHTML === '<i class="fa-solid fa-star-of-life"></i>') {
      x++;
    }
  });
  countButton.innerHTML = `0/${x}`;
}
