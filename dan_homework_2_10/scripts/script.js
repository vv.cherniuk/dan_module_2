function tabsDisplay() {
  const tabs = Array.from(document.querySelectorAll(".tabs-title"));
  for (let key in tabs) {
    tabs[key].addEventListener("click", (event) => {
      let target = event.currentTarget;

      for (let key in tabs) {
        tabs[key].classList.remove("active");
      }

      target.classList.add("active");

      //
      let targetNode = target.dataset.menu;
      let nodeNoDisplay = Array.from(
        document.querySelectorAll(".tabs-content li")
      );
      for (let key in nodeNoDisplay) {
        nodeNoDisplay[key].dataset.visibility = "none";
      }
      let nodeToDisplay = document.querySelector(
        `.tabs-content [data-menu='${targetNode}']`
      );
      nodeToDisplay.dataset.visibility = "display";
      //
    });
  }
}

tabsDisplay();



// let ul = document.querySelector(".tabs");
// ul.addEventListener("click", function (ev) {
//   console.log(ev.target.dataset.type);
//   let data = ev.target.dataset.type;
//   console.log(document.querySelector(".active-p"));
//   document.querySelector(".active-p").classList.remove("active-p");
//   console.log(document.querySelector(".active-tab"));
//   document.querySelector(".active-tab").classList.remove("active-tab");
//   console.log(document.querySelector(`[data-li = ${data}]`));
//   document.querySelector(`[data-li = ${data}]`).classList.add("active-p");
//   ev.target.classList.add("active-tab");
// });
