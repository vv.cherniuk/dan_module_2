
function filterBy(arr, varType) {
    let filterArr = arr.filter((elValue) => (typeof elValue) !== varType);
    return filterArr;
}
let userArr = ['hello', 'world', 23, '23', null];
let userType = 'string';

console.log(filterBy(userArr, userType));