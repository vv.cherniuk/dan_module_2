function dClone(cloneObject) {
  const funcsObj = {
    Object: () => {
      let clObj = {};
      for (let prop in cloneObject) {
        clObj[prop] = dClone(cloneObject[prop]);
      }
      return clObj;
    },
    Array: () => {
      return cloneObject.map((i) => {
        return dClone(i);
      });
    },
  };
  if (cloneObject.constructor.name in funcsObj) {
    return funcsObj[cloneObject.constructor.name]();
  } else {
    return cloneObject;
  }
}

let c = {
  infinity: 10,
  lock: "string",
  obj: {
    string: "1",
    last: true,
    arr: [1, 2, 3],
  },
};

a = dClone(c);
console.log(a);
