let userNumber = prompt('Enter a Number', '');
while (isNaN(userNumber)) {
    userNumber = prompt('Enter correct Number', `${userNumber}`);
}

function factorial(number) {
    if (number == 1) {
        return number;
    } else {
        return number * factorial(number - 1);
    } 
}

alert(`The factorial of your number is ${factorial(userNumber)}`);